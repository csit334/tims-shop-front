import React, { PureComponent } from 'react';
import RightContent from '../GlobalHeader/RightContent';
import styles from './index.less';

export default class TopNavHeader extends PureComponent {
  render() {
    const { theme, contentWidth, logo } = this.props;
    return (
      <div className={`${styles.head} ${theme === 'light' ? styles.light : ''}`}>
        <div
          ref={ref => {
            this.maim = ref;
          }}
          className={`${styles.main} ${contentWidth === 'Fixed' ? styles.wide : ''}`}
        >
          <div className={styles.left}>
            <div className={styles.logo} key="logo" id="logo">
              <h1>Tim's Hobbies Shop</h1>
            </div>
          </div>
          <RightContent {...this.props} />
        </div>
      </div>
    );
  }
}
