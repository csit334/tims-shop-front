import React, { PureComponent } from 'react';
import BaseMenu from '../SiderMenu/BaseMenu';
import { getFlatMenuKeys } from '../SiderMenu/SiderMenuUtils';
import styles from './index.less';

export default class TopMenu extends PureComponent {
  render() {
    const { menuData } = this.props;
    const flatMenuKeys = getFlatMenuKeys(menuData);
    return (
      <div style={{background:'#8c8c8c'}}>
        <BaseMenu {...this.props} flatMenuKeys={flatMenuKeys} className={styles.menu} />
      </div>
    );
  }
}