import React, { PureComponent } from 'react';
import Link from 'umi/link';
import styles from './index.less';
import RightContent from './RightContent';

export default class GlobalHeader extends PureComponent {
  render() {
    const { isMobile } = this.props;
    return (
      <div className={styles.header}>
        <RightContent {...this.props} />
      </div>
    );
  }
}
