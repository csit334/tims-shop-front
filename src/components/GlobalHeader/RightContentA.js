import React, { PureComponent } from 'react';
import { FormattedMessage, formatMessage } from 'umi/locale';
import { Icon, Popconfirm } from 'antd';
import styles from './index.less';
import router from 'umi/router';

function confirm() {
  router.push('/login/adminlogin')
}

export default class GlobalHeaderRightAdmin extends React.Component {
  
  render() {
    const {
      theme,
    } = this.props;

    //格式
    let className = styles.right;
    if (theme === 'dark') {
      className = `${styles.right}  ${styles.dark}`;
    }

    return (
      <div className={className}>
        
        <Popconfirm
          placement="topRight"
          title="Are you sure to log out?"
          onConfirm={confirm}
          okText="Yes"
          cancelText="No"
        >
          <span className={`${styles.action} ${styles.account}`}>
            <Icon style={{fontSize: '20px', color: 'black',}} type="logout" />
          </span>
        </Popconfirm>

      </div>
    );
  }
}
