import React, { PureComponent } from 'react';
import { FormattedMessage, formatMessage } from 'umi/locale';
import { Icon, Popconfirm } from 'antd';
import HeaderSearch from '../HeaderSearch';
import styles from './index.less';
import router from 'umi/router';

function confirm() {
  router.push('/login/user')
}

function click() {
  router.push('/account')
}

export default class GlobalHeaderRight extends React.Component {
  
  render() {
    const {
      theme,
    } = this.props;

    //格式
    let className = styles.right;
    if (theme === 'dark') {
      className = `${styles.right}  ${styles.dark}`;
    }

    return (
      <div className={className}>

        <HeaderSearch
          className={`${styles.action} ${styles.search}`}
          placeholder="Search"
          onSearch={value => {
            console.log('input', value);
          }}
          onPressEnter={value => {
            console.log('enter', value);
          }}
        />
        
        <span className={`${styles.action} ${styles.account}`}>
            <Icon onClick={click} type="user" />
        </span>
        
        <Popconfirm
          placement="topRight"
          title="Are you sure to log out?"
          onConfirm={confirm}
          okText="Yes"
          cancelText="No"
        >
          <span className={`${styles.action} ${styles.account}`}>
            <Icon type="logout" />
          </span>
        </Popconfirm>

      </div>
    );
  }
}
