import React, { PureComponent } from 'react';
import RightContentA from '../GlobalHeader/RightContentA';
import styles from './index.less';

export default class TopHeaderAdmin extends PureComponent {
  render() {
    const { theme, contentWidth, logo } = this.props;
    return (
      <div className={`${styles.head} ${theme === 'light' ? styles.light : ''}`}>
        <div
          ref={ref => {
            this.maim = ref;
          }}
          className={`${styles.main} ${contentWidth === 'Fixed' ? styles.wide : ''}`}
        >
          <div className={styles.left}>
            <div className={styles.logo} key="logo" id="logo">
              <h1>Tim's Hobbies Shop</h1>
            </div>
          </div>
          <RightContentA {...this.props} />
        </div>
      </div>
    );
  }
}
