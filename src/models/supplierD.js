import { supplierList } from '@/services/api';

export default {
  namespace: 'supplierD',

  state: {
    supplierlist: [],
  },

  effects: {
    *fetch(_, { call, put }) {
      const response = yield call(supplierList);
      yield put({
        type: 'save',
        payload: Array.isArray(response) ? response : [],
      });
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        supplierlist: action.payload,
      };
    },
  },
};