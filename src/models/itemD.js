import { itemList } from '@/services/api';

export default {
  namespace: 'itemD',

  state: {
    itemlist: [],
  },

  effects: {
    *fetch(_, { call, put }) {
      const response = yield call(itemList);
      yield put({
        type: 'save',
        payload: Array.isArray(response) ? response : [],
      });
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        itemlist: action.payload,
      };
    },
  },
};