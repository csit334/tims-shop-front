import { memberList } from '@/services/api';

export default {
  namespace: 'memberD',

  state: {
    memberlist: [],
  },

  effects: {
    *fetch(_, { call, put }) {
      const response = yield call(memberList);
      yield put({
        type: 'save',
        payload: Array.isArray(response) ? response : [],
      });
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        memberlist: action.payload,
      };
    },
  },
};