export default {
  'menu.welcome': 'Welcome',
  'menu.more-blocks': 'More Blocks',

  'menu.account.center': 'Account Center',
  'menu.account.settings': 'Account Settings',
  'menu.account.trigger': 'Trigger Error',
  'menu.account.logout': 'Logout',

  'menu.Sale Page': 'Sale Page',
  'menu.Add Supply Transaction': 'Add Supply Transaction',
  'menu.SaleOrderDetails': 'Sales Order Details',
  'menu.SalesOrder': 'Sales Order',
  'menu.SupplyOrder': 'Supply Order',
  'menu.SupplyOrderDetails': 'Supply Order Details',
  'menu.ItemsList': 'Items List',
  'menu.ItemDetail': 'Item Detail',
  'menu.SuppliersList': 'Suppliers List',
  'menu.SupplierDetail': 'Suppliers Detail',
  'menu.CustomersList': 'Customers List',
  'menu.CustomerDetail': 'Customers Detail',
  'menu.StoreControl': 'Store Control',
};
