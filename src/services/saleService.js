
import config from './config';

export async function getSalesOrder() {
    return (await fetch(
      `${config.server}/orders`
    )).json()
}

export async function getSalesOrderDetail(id) {
  return (await fetch(
    `${config.server}/orders/${id}`
  )).json()
}

export async function postSalesOrder(data) {
  return (await fetch(
    `${config.server}/orders`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    }
  )).json()
}

export async function updateSalesOrder(data) {
  return (await fetch(
    `${config.server}/orders`, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    }
  )).json()
}

export async function deleteSalesOrder(id) {
  return (await fetch(
    `${config.server}/orders/${id}`, {
      method: 'DELETE',
    }
  )).json()
}