
import config from './config';

export async function getCustomers() {
    return (await fetch(
      `${config.server}/customers`
    )).json()
}

export async function getCustomerDetail(id) {
  return (await fetch(
    `${config.server}/customers/${id}`
  )).json()
}

export async function postCustomers(data) {
  return (await fetch(
    `${config.server}/customers`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    }
  )).json()
}

export async function updateCustomers(data) {
  return (await fetch(
    `${config.server}/customers`, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    }
  )).json()
}

export async function deleteCustomers(id) {
  return (await fetch(
    `${config.server}/customers/${id}`, {
      method: 'DELETE',
    }
  )).json()
}