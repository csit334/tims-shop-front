import request from '@/utils/request';
import { stringify } from 'qs';
import config from './config';


export async function itemList(){
    return request ('/item/itemD/getItem');
}

export async function supplierList(){
  return request ('/supplier/supplierD/getSupplier');
}

export async function memberList(){
  return request ('/member/memberD/getMember');
}

// supplier
export async function getSuppliers(){
  return request (config.server + '/suppliers');
}



export async function queryFakeList() {
    //return request(`/api/fake_list?${stringify(params)}`);
    return request('/api/fake_list');
  }
  
  export async function removeFakeList(params) {
    const { count = 5, ...restParams } = params;
    return request(`/api/fake_list?count=${count}`, {
      method: 'POST',
      body: {
        ...restParams,
        method: 'delete',
      },
    });
  }
  
  export async function addFakeList(params) {
    const { count = 5, ...restParams } = params;
    return request(`/api/fake_list?count=${count}`, {
      method: 'POST',
      body: {
        ...restParams,
        method: 'post',
      },
    });
  }
  
  export async function updateFakeList(params) {
    const { count = 5, ...restParams } = params;
    return request(`/api/fake_list?count=${count}`, {
      method: 'POST',
      body: {
        ...restParams,
        method: 'update',
      },
    });
  }
  