
import config from './config';

export async function getItems() {
    return (await fetch(
      `${config.server}/model-items`
    )).json()
}

export async function getItemDetail(id) {
  return (await fetch(
    `${config.server}/model-items/${id}`
  )).json()
}

export async function postItems(data) {
  return (await fetch(
    `${config.server}/model-items`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    }
  )).json()
}

export async function updateItems(data) {
  return (await fetch(
    `${config.server}/model-items`, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    }
  )).json()
}

export async function deleteItem(id) {
  return (await fetch(
    `${config.server}/model-items/${id}`, {
      method: 'DELETE',
    }
  )).json()
}