
import config from './config';

export async function getAdmins() {
    return (await fetch(
      `${config.server}/admins`
    )).json()
}

export async function getAdminDetail(id) {
  return (await fetch(
    `${config.server}/admins/${id}`
  )).json()
}

export async function postAdmins(data) {
  return (await fetch(
    `${config.server}/admins`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    }
  )).json()
}

export async function updateAdmins(data) {
  return (await fetch(
    `${config.server}/admins`, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    }
  )).json()
}

export async function deleteAdmins(id) {
  return (await fetch(
    `${config.server}/admins/${id}`, {
      method: 'DELETE',
    }
  )).json()
}