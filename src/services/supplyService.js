
import config from './config';

export async function getSupplyOrder() {
    return (await fetch(
      `${config.server}/supplier-orders`
    )).json()
}

export async function getSupplyOrderDetail(id) {
  return (await fetch(
    `${config.server}/supplier-orders/${id}`
  )).json()
}

export async function postSupplyOrder(data) {
  return (await fetch(
    `${config.server}/supplier-orders`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    }
  )).json()
}

export async function updateSupplyOrder(data) {
  return (await fetch(
    `${config.server}/supplier-orders`, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    }
  )).json()
}

export async function deleteSupplyOrder(id) {
  return (await fetch(
    `${config.server}/supplier-orders/${id}`, {
      method: 'DELETE',
    }
  )).json()
}