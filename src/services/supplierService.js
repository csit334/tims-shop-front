
import config from './config';

export async function getSuppliers() {
    return (await fetch(
      `${config.server}/suppliers`
    )).json()
}

export async function getSupplierDetail(id) {
  return (await fetch(
    `${config.server}/suppliers/${id}`
  )).json()
}

export async function postSuppliers(data) {
  return (await fetch(
    `${config.server}/suppliers`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    }
  )).json()
}

export async function updateSuppliers(data) {
  return (await fetch(
    `${config.server}/suppliers`, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    }
  )).json()
}

export async function deleteSupplier(id) {
  return (await fetch(
    `${config.server}/suppliers/${id}`, {
      method: 'DELETE',
    }
  )).json()
}