import React, { Suspense } from 'react';
import { Layout } from 'antd';
import DocumentTitle from 'react-document-title';
import { connect } from 'dva';
import { ContainerQuery } from 'react-container-query';
import classNames from 'classnames';
import logo from '../assets/logo.svg';
import AdminHeader from './AdminHeader';
import Context from './MenuContext';
import SiderMenu from '@/components/SiderMenu';
import styles from './BasicLayout.less';

const { Content } = Layout;

const query = {
  'screen-xs': {
    maxWidth: 575,
  },
  'screen-sm': {
    minWidth: 576,
    maxWidth: 767,
  },
  'screen-md': {
    minWidth: 768,
    maxWidth: 991,
  },
  'screen-lg': {
    minWidth: 992,
    maxWidth: 1199,
  },
  'screen-xl': {
    minWidth: 1200,
    maxWidth: 1599,
  },
  'screen-xxl': {
    minWidth: 1600,
  },
};

class AdminLayout extends React.Component {
  componentDidMount() {
    const {
      dispatch,
      route: { routes, path, authority },
    } = this.props;
    dispatch({
      type: 'user/fetchCurrent',
    });
    dispatch({
      type: 'setting/getSetting',
    });
    dispatch({
      type: 'menu/getMenuData',
      payload: { routes, path, authority },
    });
  }

  render() {
    const {
      navTheme,
      layout: PropsLayout,
      children,
      menuData,
      fixedHeader,
    } = this.props;

    const contentStyle = !fixedHeader ? { paddingTop: 0 } : {};

    const layout = (
      <Layout>
        
        <AdminHeader/>
          
        <Layout>
          
          <SiderMenu
            logo={logo}
            //theme={navTheme}
            className={styles.b}
            menuData={menuData}
            {...this.props}
          />
          
          <Content className={styles.content} style={contentStyle}>
            {children}
          </Content>

        </Layout>
      </Layout>
    );
    return (
      <React.Fragment>
        <DocumentTitle title="Tim's Hobbies Shop">
          <ContainerQuery query={query}>
            {params => (
              <Context.Provider value="Tim">
                <div className={classNames(params)}>{layout}</div>
              </Context.Provider>
            )}
          </ContainerQuery>
        </DocumentTitle>
      </React.Fragment>
    );
  }
}

export default connect(({ setting, menu }) => ({
  layout: setting.layout,
  menuData: menu.menuData,
  ...setting,
}))(props => ( <AdminLayout {...props}  /> ));
