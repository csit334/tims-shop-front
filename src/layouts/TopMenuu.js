import React, { PureComponent } from 'react';
import { Layout, message } from 'antd';
import Animate from 'rc-animate';
import { connect } from 'dva';
import TopMenuV from '@/components/TopMenu';
import styles from './Header.less';

const { Header } = Layout;

class TopMenuView extends PureComponent {
  state = {
    visible: true,
  };

  render() {
    const { handleMenuCollapse, setting } = this.props;
    const { navTheme, layout, fixedHeader } = setting;
    const { visible } = this.state;
    const width = '100%';
    return (
      <TopMenuV
        theme={navTheme}
        mode="horizontal"
        {...this.props}
      />
    );
  }
}

export default connect(({ setting }) => ({
  setting,
}))(TopMenuView);
