import React from 'react';
import { Button, Card } from 'antd';
import styles from './UserPage.less';

const { Meta } = Card;

const gridStyle = {
    width: '25%',
    textAlign: 'center',
  };

class UserAccount extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cardListV: [
        {
          key: '0',
          item_name: 'red car',
          price: '$100',
        },
        {
          key: '1',
          item_name: 'blue car',
          price: '$100',
        },
      ],
      count: 2,
    };
  }

  handleDelete = key => {
    const cardListV = [...this.state.cardListV];
    this.setState({ cardListV: cardListV.filter(item => item.key !== key) });
  };

  render() {
    const { cardListV } = this.state;

    return (
      <div>

        <Card 
            title="Your Favorite List"
        >
        {cardListV.map(item => (

            <Card.Grid style={gridStyle} key={item.key}>
                <Card
                    hoverable
                    cover={<img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />}
                >
                    <Meta style={{ marginTop:'10px' }} title={item.item_name} description={item.price} />
                    <Button 
                        style={{ marginBottom:'10px', marginTop:'10px' }}
                        type="primary"
                        onClick={() => this.handleDelete(item.key)}
                    >
                        Remove
                    </Button>
                </Card>
            </Card.Grid>

         ))}

        </Card>

      </div>
    );
  }
}

export default UserAccount;
