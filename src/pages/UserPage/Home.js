import React, { Component } from 'react';
import 'antd/dist/antd.css';
import styles from './UserPage.less';
import { Carousel, Card, Row, Col, Icon } from 'antd';
import router from 'umi/router';

function Transf() {
  router.push('/itemD')
}

const { Meta } = Card;

class Home extends Component{

  render(){

    return(
      <div>
        
      <Carousel className={styles.carousel} autoplay>
        <div>
          <Icon onClick={Transf} type='apple' theme="filled" style={{ fontSize: '450px'}} />
        </div>
        <div>
          <Icon onClick={Transf} type='android' theme="filled" style={{ fontSize: '450px'}} />
        </div>
        <div>
          <Icon onClick={Transf} type='gitlab' theme="filled" style={{ fontSize: '450px'}} />
        </div>
        <div>
          <Icon onClick={Transf} type='wechat' theme="filled" style={{ fontSize: '450px'}} />
        </div>
      </Carousel>

      <div className={styles.cardB}>
        <Row type="flex" justify="center">

          <Col span={6}>
            <Card
              onClick={Transf}
              hoverable
              style={{ marginTop: '10px', width: 240 }}
              cover={<img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />}
            >
              <Meta title="Europe Street beat" description="www.instagram.com" />
            </Card>
          </Col>

          <Col span={6}>
            <Card
              onClick={Transf}
              hoverable
              style={{ marginTop: '10px', width: 240 }}
              cover={<img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />}
            >
              <Meta title="Europe Street beat" description="www.instagram.com" />
            </Card>
          </Col>

          <Col span={6}>
            <Card
              onClick={Transf}
              hoverable
              style={{ marginTop: '10px', width: 240 }}
              cover={<img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />}
            >
              <Meta title="Europe Street beat" description="www.instagram.com" />
            </Card>
          </Col>

          <Col span={6}>
            <Card
              onClick={Transf}
              hoverable
              style={{ marginTop: '10px', width: 240 }}
              cover={<img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />}
            >
              <Meta title="Europe Street beat" description="www.instagram.com" />
            </Card>
          </Col>

        </Row>
      </div>

      </div>
    );
  }
}
export default Home;
          