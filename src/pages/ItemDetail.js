import React, { Component } from 'react';
import 'antd/dist/antd.css';
import styles from './ItemDetail.less';
import { Icon, Row, Col, Select, Button, Tabs } from 'antd';

const TabPane = Tabs.TabPane;
const Option = Select.Option;

function onChange(value) {
  console.log(`selected ${value}`);
}
  
function onBlur() {
  console.log('blur');
}
  
function onFocus() {
  console.log('focus');
}
  
function onSearch(val) {
  console.log('search:', val);
}

function callback(key) {
  console.log(key);
}

class ItemDetail extends Component{

    name = {};
    backGC = {};

    constructor(){
        super();
        this.state = {
            typeN: 0,
            id: 0,
            c: 1,
        };
        ["apple", "android", "qq", "wechat"].forEach((value, i) => {
            this.name[i] = value;
        });
        ["#fa8c16", "#1890ff"].forEach((value2, a) => {
            this.backGC[a] = value2;
        });
    }

    onChangeA(idd){
        this.setState({ id: idd });
    };
    onChangeB(aa){
        this.setState({ c: aa });
    };

  render(){
    const { typeN, id, c }=this.state;

    return(
    <div>
    
      <div className={styles.title}>
        <h1>Item Name</h1>
      </div>

      <div className={styles.body}>
        <Row gutter={24}>
            <Col span={12}>
                <div className={styles.divA}>
                    <Icon 
                        type={this.name[id]}
                        style={{ marginTop: '10px' , fontSize: '400px', background: '#fff7e6'}}
                    />
                    <Row type="flex" justify="center">
                        <Col span={4}>
                            <Icon type={this.name[0]}
                                onClick={() => this.onChangeA(0)}
                                style={{ marginTop: '20px' , fontSize: '100px', background: '#fff7e6'}}
                            />
                        </Col>
                        <Col span={4}>
                            <Icon type={this.name[1]}
                                onClick={() => this.onChangeA(1)}
                                style={{ marginTop: '20px' , fontSize: '100px', background: '#fff7e6'}}
                            />
                        </Col>
                        <Col span={4}>
                            <Icon type={this.name[2]}
                                onClick={() => this.onChangeA(2)}
                                style={{ marginTop: '20px' , fontSize: '100px', background: '#fff7e6'}}
                            />
                        </Col>
                        <Col span={4}>
                            <Icon type={this.name[3]}
                                onClick={() => this.onChangeA(3)}
                                style={{ marginTop: '20px' , fontSize: '100px', background: '#fff7e6'}}
                            />
                        </Col>
                    </Row>
                </div>
            </Col>
           
            <Col span={12}>
                <div className={styles.divB}>
                    <h1>Brand:</h1>
                    <h1>Item Number:</h1>
                    <h1>Location:</h1>
                    <h1>Options: 
                        <span style={{ marginLeft:'50px' }}>
                            <Select
                                showSearch
                                style={{ width: 200 }}
                                placeholder="Select a Color"
                                optionFilterProp="children"
                                onChange={onChange}
                                onFocus={onFocus}
                                onBlur={onBlur}
                                onSearch={onSearch}
                                filterOption={(input, option) =>
                                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                }
                            >
                                <Option value="red">Red</Option>
                                <Option value="blue">Blue</Option>
                                <Option value="black">Black</Option>
                            </Select>    
                        </span>
                    </h1>
                    <h2>$200  <span style={{ fontSize:'20px' }}>Excluding GST: $199</span></h2>
                    <Button 
                        style={{ marginLeft:'10px', height: '40px', backgroundColor: this.backGC[c] }}
                        type="primary"
                        onClick={() => this.onChangeB(0)}
                    >
                        Add To Favorite
                    </Button>
                </div>
            </Col>
        </Row>
      </div>

      <div>
            <Tabs size={'large'} style={{fontSize:'50px'}} defaultActiveKey="1" onChange={callback}>
                <TabPane tab="Description" key="1">
                    Content of Tab Pane 1
                </TabPane>
                <TabPane tab="Specification" key="2">
                    Content of Tab Pane 2
                </TabPane>
                <TabPane tab="Review" key="3">
                    Content of Tab Pane 3
                </TabPane>
                <TabPane tab="Standard Part" key="4">
                    Content of Tab Pane 4
                </TabPane>
                <TabPane tab="Optional Part" key="5">
                    Content of Tab Pane 5
                </TabPane>
            </Tabs>
      </div>

    </div>
    );
  }
}
export default ItemDetail;