
import React from 'react';
import { Table, Input, Popconfirm, Divider } from 'antd';
import styles from './AdminPage.less';

import {getSalesOrder, deleteSalesOrder} from '@/services/saleService';

import { push } from 'react-router-redux';
import router from 'umi/router';

const Search = Input.Search;

class SalesOrder extends React.Component {
  constructor(props) {
    super(props);
    this.columns = [
      {
        title: 'ID',
        dataIndex: 'id',
        width: '4%',
        sorter: (a, b) => a.id - b.id,
      },
      {
        title: 'Customer ID',
        dataIndex: 'customerId',
        width: '10%',
      },
      {
        title: 'Price',
        dataIndex: 'price',
        width: '10%',
        sorter: (a, b) => a.price - b.price,
      },
      {
        title: 'State',
        dataIndex: 'state',
        width: '12%',
      },
      {
        title: 'Date Time',
        dataIndex: 'datetime',
        width: '15%',
        sorter: (a, b) => a.datetime - b.datetime,
      },
      {
        title: 'Operation',
        dataIndex: 'operation',
        width: '10%',
        render: (text, record) =>
          this.state.dataSource.length >= 1 ? (
            <span>
              <Popconfirm title="Sure to delete?" onConfirm={() => deleteSalesOrder(record.id).then(() => this.componentDidMount())}>
                <a href="javascript:;">Delete</a>
              </Popconfirm>
              <Divider type="vertical" />
              <a onClick={() => {router.push(`/admin/SalesOD/${record.id}`)}}>Detail</a>
            </span>
          ) : null,
      },
    ];

    this.state = {
      dataSource: [],
      saveData: [],
    };

  }

  componentDidMount() {
    const {dataSource} = this.state;
    this.loadData();
  }

  async loadData(){
    let data = await getSalesOrder()
    this.setState({
      dataSource: data.data,
      saveData: data.data,
    })
  }

  onSearch = (value) =>{
    if(!value){
      this.setState({
        dataSource: this.state.saveData, 
      });
    }
    else{
      this.setState({
        dataSource: this.state.saveData.filter(item => item.customerId == value),
      });
    }
  }

  render() {
    const { dataSource } = this.state;
    const columns = this.columns.map(col => {
      return {
        ...col,
        onCell: record => ({
          record,
          dataIndex: col.dataIndex,
          title: col.title,
        }),
      };
    });

    return (
      <div className={styles.divLayout}>
        <h1>Sales Order List</h1>
      <div class="ant-card ant-card-bordered">
        
        <Search 
          style={{marginTop: '20px', marginLeft: '20px', marginRight: '20px', width: '300px'}} 
          placeholder="Search By Customer ID" 
          onSearch={
            value => this.onSearch(value)
          } 
          enterButton
        />
        
        <Table style={{marginTop: '20px', marginLeft: '20px', marginRight: '20px'}}
          rowClassName={() => 'editable-row'}
          bordered
          dataSource={dataSource}
          columns={columns}
        />

      </div>
      </div>
    );
  }
}

export default SalesOrder;