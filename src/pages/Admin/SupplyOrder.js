
import React from 'react';
import { Table, Input, Button, Popconfirm, Divider } from 'antd';
import styles from './AdminPage.less';

import {getSupplyOrder, deleteSupplyOrder} from '@/services/supplyService';

import { push } from 'react-router-redux';
import router from 'umi/router';

const Search = Input.Search;

class Supplyorder extends React.Component {
  constructor(props) {
    super(props);
    this.columns = [
      {
        title: 'ID',
        dataIndex: 'id',
        width: '4%',
        sorter: (a, b) => a.id - b.id,
      },
      {
        title: 'Admin ID',
        dataIndex: 'adminId',
        width: '8%',
      },
      {
        title: 'Supplier ID',
        dataIndex: 'supplierId',
        width: '8%',
      },
      {
        title: 'Price',
        dataIndex: 'price',
        width: '8%',
        sorter: (a, b) => a.price - b.price,
      },
      {
        title: 'Date Time',
        dataIndex: 'datetime',
        width: '10%',
        sorter: (a, b) => a.datetime - b.datetime,
      },
      {
        title: 'Operation',
        dataIndex: 'operation',
        width: '8%',
        render: (text, record) =>
          this.state.dataSource.length >= 1 ? (
            <span>
              <Popconfirm title="Sure to delete?" onConfirm={() => deleteSupplyOrder(record.id).then(() => this.componentDidMount())}>
                <a href="javascript:;">Delete</a>
              </Popconfirm>
              <Divider type="vertical" />
              <a onClick={() => {router.push(`/admin/SupplyOD/${record.id}`)}}>Detail</a>
            </span>
          ) : null,
      },
    ];

    this.state = {
      dataSource: [],
      saveData: [],
    };

  }

  componentDidMount() {
    const {dataSource} = this.state;
    this.loadData();
  }

  async loadData(){
    let data = await getSupplyOrder()
    this.setState({
      dataSource: data.data,
      saveData: data.data,
    })
  }

  onSearch = (value) =>{
    if(!value){
      this.setState({
        dataSource: this.state.saveData, 
      });
    }
    else{
      this.setState({
        dataSource: this.state.saveData.filter(item => item.supplierId == value),
      });
    }
  }

  render() {
    const { dataSource } = this.state;
    const columns = this.columns.map(col => {
      return {
        ...col,
        onCell: record => ({
          record,
          dataIndex: col.dataIndex,
          title: col.title,
        }),
      };
    });

    return (
      <div className={styles.divLayout}>
        <h1>Supply Order List</h1>
      <div class="ant-card ant-card-bordered">
        
        <Search 
          style={{marginTop: '20px', marginLeft: '20px', marginRight: '20px', width: '300px'}} 
          placeholder="Search By Supplier ID" 
          onSearch={
            value => this.onSearch(value)
          } 
          enterButton
        />
        
        <Table style={{marginTop: '20px', marginLeft: '20px', marginRight: '20px'}}
          rowClassName={() => 'editable-row'}
          bordered
          dataSource={dataSource}
          columns={columns}
        />

      </div>
      </div>
    );
  }
}

export default Supplyorder;