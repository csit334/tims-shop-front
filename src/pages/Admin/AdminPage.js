
import React from 'react';
import { Typography,Icon } from 'antd';

const { Title } = Typography;

export default () => (
  <Title style={{ textAlign: 'center', marginTop: '100px'}}>
    <Icon style={{fontSize:'600px'}} type="sketch" />
  </Title>
);
