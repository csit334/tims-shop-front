
import React from 'react';
import { Form, Input, Button, Modal } from 'antd';
import {getCustomerDetail, postCustomers, updateCustomers} from '@/services/customerService';
import { supportsHistory } from 'history/DOMUtils';
import { push } from 'react-router-redux';

class CustomerDetails extends React.Component {

    constructor(props) {
        super(props);

        this.update = true;//
        this.buttonName = 'Update';
        
        this.state = {
            dataSource: {},
            addressData: {},
            label: {
                addressId: 'Address ID',
                firstName: 'First Name',
                lastName: 'Last Name',
                username: 'Username',
                phoneNumber: 'Phone Number',
                email: 'Email',
                creditLine: 'Credit Line',
                balance: 'Balance',
                member: 'Member',
                joinAt: 'Join Date',

                unitNumber: 'Unit Number',
                streetNumber: 'Street Number',
                streetName: "Street Name",
                suburb: "Suburb",
                state: "State",
                postcode: "Postcode",
                country: "Country",

            },
        };

        //
        if(this.props.match.params.id){
            this.update = true;
            this.buttonName = 'Update';
        }
        else {
            this.update = false;
            this.buttonName = 'Create';
        }

    }

    componentDidMount() {
        if(this.update){ //
            this.loadData();
        }
    }

    handleSubmit = () => {
        var timestamp = new Date().getTime();
        let data = {
            address: {
                country: this.props.form.getFieldValue('country'),
                postcode: this.props.form.getFieldValue('postcode'),
                state: this.props.form.getFieldValue('state'),
                streetName: this.props.form.getFieldValue('streetName'),
                streetNumber: this.props.form.getFieldValue('streetNumber'),
                suburb: this.props.form.getFieldValue('suburb'),
                unitNumber: this.props.form.getFieldValue('unitNumber'),
            },
                addressId: this.props.form.getFieldValue('addressId'),
                firstName: this.props.form.getFieldValue('firstName'),
                lastName: this.props.form.getFieldValue('lastName'),
                username: this.props.form.getFieldValue('username'),
                phoneNumber: this.props.form.getFieldValue('phoneNumber'),
                email: this.props.form.getFieldValue('email'),
                creditLine: this.props.form.getFieldValue('creditLine'),
                balance: this.props.form.getFieldValue('balance'),
                member: this.props.form.getFieldValue('member'),
                joinAt: timestamp,
        };
        console.log(data);

        if(this.update){ // update
            data.id = this.state.dataSource.id;
            updateCustomers(data).then(
                this.props.history.push('/admin/CustomersL')
            );
            Modal.success({
                title: 'Successful',
                content: 'Your Update Is Successful!',
            });
        }
        else { // create
            postCustomers(data).then(
                this.props.history.push('/admin/CustomersL')
            );
        }

        this.componentDidMount();
    }

    checkError = e => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
          if (!err) {
            console.log('Received values of form: ', values);
            Modal.success({
                title: 'Successful',
                content: 'Your Create Is Successful!',
            });
          }
        });
    };

    async loadData(){
        let data = await getCustomerDetail(this.props.match.params.id);
        console.log(data.data);
        this.setState({
            dataSource: data.data,
            addressData: data.data.address,
        });
    }

    render(){

        const formItemLayout = {
            labelCol: {
              xs: { span: 24 },
              sm: { span: 8 },
            },
            wrapperCol: {
              xs: { span: 24 },
              sm: { span: 16 },
            },
        };

        const tailFormItemLayout = {
            wrapperCol: {
              xs: {
                span: 24,
                offset: 0,
              },
              sm: {
                span: 16,
                offset: 8,
              },
            },
        };

        const { dataSource, addressData, label } = this.state;
        const { getFieldDecorator } = this.props.form;
        return (
            <div>
            <Form {...formItemLayout} onSubmit={this.checkError} >

                <Form.Item label={label.addressId}>
                    {getFieldDecorator('addressId', {
                        initialValue: dataSource.addressId,
                        rules: [
                            {
                                required: true,
                                message: 'Please confirm your addressId!',
                            },
                        ],
                    })(<Input style={{width: '50%'}}/>)}
                </Form.Item>

                <Form.Item label={label.firstName}>
                    {getFieldDecorator('firstName', {
                        initialValue: dataSource.firstName,
                        rules: [
                            {
                                required: true,
                                message: 'Please confirm your firstName!',
                            },
                        ],
                    })(<Input style={{width: '50%'}}/>)}
                </Form.Item>

                <Form.Item label={label.lastName}>
                    {getFieldDecorator('lastName', {
                        initialValue: dataSource.lastName,
                        rules: [
                            {
                                required: true,
                                message: 'Please confirm your lastName!',
                            },
                        ],
                    })(<Input style={{width: '50%'}}/>)}
                </Form.Item>

                <Form.Item label={label.username}>
                    {getFieldDecorator('username', {
                        initialValue: dataSource.username,
                        rules: [
                            {
                                required: true,
                                message: 'Please confirm your username!',
                            },
                        ],
                    })(<Input style={{width: '50%'}}/>)}
                </Form.Item>

                <Form.Item label={label.phoneNumber}>
                    {getFieldDecorator('phoneNumber', {
                        initialValue: dataSource.phoneNumber,
                        rules: [
                            {
                                required: true,
                                message: 'Please confirm your phoneNumber!',
                            },
                        ],
                    })(<Input style={{width: '50%'}}/>)}
                </Form.Item>

                <Form.Item label={label.email}>
                    {getFieldDecorator('email', {
                        initialValue: dataSource.email,
                        rules: [
                            {
                                required: true,
                                message: 'Please confirm your email!',
                            },
                        ],
                    })(<Input style={{width: '50%'}}/>)}
                </Form.Item>

                <Form.Item label={label.creditLine}>
                    {getFieldDecorator('creditLine', {
                        initialValue: dataSource.creditLine,
                        rules: [
                            {
                                required: true,
                                message: 'Please confirm your creditLine!',
                            },
                        ],
                    })(<Input style={{width: '50%'}}/>)}
                </Form.Item>

                <Form.Item label={label.balance}>
                    {getFieldDecorator('balance', {
                        initialValue: dataSource.balance,
                        rules: [
                            {
                                required: true,
                                message: 'Please confirm your balance!',
                            },
                        ],
                    })(<Input style={{width: '50%'}}/>)}
                </Form.Item>

                <Form.Item label={label.member}>
                    {getFieldDecorator('member', {
                        initialValue: dataSource.member,
                        rules: [
                            {
                                required: true,
                                message: 'Please confirm your member!',
                            },
                        ],
                    })(<Input style={{width: '50%'}}/>)}
                </Form.Item>

                <Form.Item label="Address Details:" />
                <Form.Item label={label.unitNumber}>
                    {getFieldDecorator('unitNumber', {
                        initialValue: addressData.unitNumber,
                        rules: [
                            {
                                required: true,
                                message: 'Please confirm your Unit Number!',
                            },
                        ],
                    })(<Input style={{width: '50%'}}/>)}
                </Form.Item>

                <Form.Item label={label.streetNumber}>
                    {getFieldDecorator('streetNumber', {
                        initialValue: addressData.streetNumber,
                        rules: [
                            {
                                required: true,
                                message: 'Please confirm your Street Number!',
                            },
                        ],
                    })(<Input style={{width: '50%'}}/>)}
                </Form.Item>

                <Form.Item label={label.streetName}>
                    {getFieldDecorator('streetName', {
                        initialValue: addressData.streetName,
                        rules: [
                            {
                                required: true,
                                message: 'Please confirm your Street Name!',
                            },
                        ],
                    })(<Input style={{width: '50%'}}/>)}
                </Form.Item>

                <Form.Item label={label.suburb}>
                    {getFieldDecorator('suburb', {
                        initialValue: addressData.suburb,
                        rules: [
                            {
                                required: true,
                                message: 'Please confirm your Suburb!',
                            },
                        ],
                    })(<Input style={{width: '50%'}}/>)}
                </Form.Item>

                <Form.Item label={label.state}>
                    {getFieldDecorator('state', {
                        initialValue: addressData.state,
                        rules: [
                            {
                                required: true,
                                message: 'Please confirm your State!',
                            },
                        ],
                    })(<Input style={{width: '50%'}}/>)}
                </Form.Item>

                <Form.Item label={label.postcode}>
                    {getFieldDecorator('postcode', {
                        initialValue: addressData.postcode,
                        rules: [
                            {
                                required: true,
                                message: 'Please confirm your Postcode!',
                            },
                        ],
                    })(<Input style={{width: '50%'}}/>)}
                </Form.Item>

                <Form.Item label={label.country}>
                    {getFieldDecorator('country', {
                        initialValue: addressData.country,
                        rules: [
                            {
                                required: true,
                                message: 'Please confirm your Country!',
                            },
                        ],
                    })(<Input style={{width: '50%'}}/>)}
                </Form.Item>

                <Form.Item {...tailFormItemLayout}>
                    <Button style={{width: '50%'}} onClick={this.handleSubmit} type="primary" htmlType="submit"> {this.buttonName} </Button>
                </Form.Item>
            </Form>

            </div>
        );
    }
}

export default Form.create()(CustomerDetails);
