
import React from 'react';
import { Table, Input, Button, Popconfirm, Divider } from 'antd';
import styles from './AdminPage.less';

import {getSuppliers, deleteSupplier} from '@/services/supplierService';

import { push } from 'react-router-redux';
import router from 'umi/router';

const Search = Input.Search;

class SuppliersList extends React.Component {
  constructor(props) {
    super(props);
    this.columns = [
      {
        title: 'ID',
        dataIndex: 'id',
        width: '4%',
        sorter: (a, b) => a.id - b.id,
      },
      {
        title: 'Supplier Name',
        dataIndex: 'name',
        width: '12.5%',
      },
      {
        title: 'Delivery Details',
        dataIndex: 'deliveryDetails',
        width: '15%',
        filters: [
          {
            text: 'Delivery',
            value: 'Delivery',
          },
          {
            text: 'Pick up',
            value: 'Pick up',
          },
        ],
        filterMultiple: false,
        onFilter: (value, record) => record.deliveryDetails.indexOf(value) === 0,
      },
      {
        title: 'Contact Persons Name',
        dataIndex: 'contactPersons',
        width: '12.5%',
      },
      {
        title: 'Credit',
        dataIndex: 'creditLine',
        width: '8%',
      },
      {
        title: 'Operation',
        dataIndex: 'operation',
        width: '8%',
        render: (text, record) =>
          this.state.dataSource.length >= 1 ? (
            <span>
              <Popconfirm title="Sure to delete?" onConfirm={() => deleteSupplier(record.id).then(() => this.componentDidMount())}>
                <a href="javascript:;">Delete</a>
              </Popconfirm>
              <Divider type="vertical" />
              <a onClick={() => {router.push(`/admin/SupplierD/${record.id}`)}}>Edit</a>
            </span>
          ) : null,
      },
    ];

    this.state = {
      dataSource: [],
      saveData: [],
    };

  }

  componentDidMount() {
    const {dataSource} = this.state;
    this.loadData();
  }

  // Create page
  handleCreate = () =>{
    router.push('/admin/SupplierD');
  };

  async loadData(){
    let data = await getSuppliers()
    console.log(data);
    this.setState({
      dataSource: data.data,
      saveData: data.data,
    })
  }

  onSearch = (value) =>{
    if(!value){
      this.setState({
        dataSource: this.state.saveData, 
      });
    }
    else{
      this.setState({
        dataSource: this.state.saveData.filter(item => item.name == value),
      });
    }
  }

  render() {
    const { dataSource } = this.state;
    const columns = this.columns.map(col => {
      return {
        ...col,
        onCell: record => ({
          record,
          dataIndex: col.dataIndex,
          title: col.title,
        }),
      };
    });

    return (
      <div className={styles.divLayout}>
        <h1>Suppliers List</h1>
      <div class="ant-card ant-card-bordered">
        
        <Search 
          style={{marginTop: '20px', marginLeft: '20px', marginRight: '20px', width: '300px'}} 
          placeholder="Search By Supplier Name" 
          onSearch={
            value => this.onSearch(value)
          }
          enterButton
        />
        
        <Table style={{marginTop: '20px', marginLeft: '20px', marginRight: '20px'}}
          rowClassName={() => 'editable-row'}
          bordered
          dataSource={dataSource}
          columns={columns}
        />

        <div style={{textAlign:'center'}}>
          <Button onClick={this.handleCreate} style={{ marginBottom: '20px', marginTop: 16, width: '300px' }}>
            Create New Supplier
          </Button>
        </div>

      </div>
      </div>
    );
  }
}

export default SuppliersList;

