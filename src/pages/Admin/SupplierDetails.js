
import React from 'react';
import { Form, Input, Button, Modal } from 'antd';
import {getSupplierDetail, postSuppliers, updateSuppliers} from '@/services/supplierService';
import { supportsHistory } from 'history/DOMUtils';
import { push } from 'react-router-redux';

class SuppliersDetails extends React.Component {

    constructor(props) {
        super(props);

        this.update = true;//
        this.buttonName = 'Update';
        
        this.state = {
            dataSource: {},
            addressData: {},
            label: {
                name: 'Name',
                deliveryDetails: 'Delivery Details',
                contactPersons: 'Contact Persons',
                creditLine: 'Credit Line',

                unitNumber: 'Unit Number',
                streetNumber: 'Street Number',
                streetName: "Street Name",
                suburb: "Suburb",
                state: "State",
                postcode: "Postcode",
                country: "Country",
            },
        };

        //
        if(this.props.match.params.id){
            this.update = true;
            this.buttonName = 'Update';
        }
        else {
            this.update = false;
            this.buttonName = 'Create';
        }

    }

    componentDidMount() {
        if(this.update){ //
            this.loadData();
        }
    }

    handleSubmit = () => {
        let data = {
            address: {
                country: this.props.form.getFieldValue('country'),
                postcode: this.props.form.getFieldValue('postcode'),
                state: this.props.form.getFieldValue('state'),
                streetName: this.props.form.getFieldValue('streetName'),
                streetNumber: this.props.form.getFieldValue('streetNumber'),
                suburb: this.props.form.getFieldValue('suburb'),
                unitNumber: this.props.form.getFieldValue('unitNumber'),
            },
            contactPersons: this.props.form.getFieldValue('contactPersons'),
            creditLine: this.props.form.getFieldValue('creditLine'),
            deliveryDetails: this.props.form.getFieldValue('deliveryDetails'),
            name: this.props.form.getFieldValue('name'),
        };
        //console.log(data);

        if(this.update){ // update
            data.id = this.state.dataSource.id;
            updateSuppliers(data).then(
                this.props.history.push('/admin/SuppliersL')
            );
            Modal.success({
                title: 'Successful',
                content: 'Your Update Is Successful!',
            });
            
        }
        else { // create
            postSuppliers(data).then(
                this.props.history.push('/admin/SuppliersL')
            );
        }

        this.componentDidMount();
    }

    checkError = e => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
          if (!err) {
            console.log('Received values of form: ', values);
            Modal.success({
                title: 'Successful',
                content: 'Your Create Is Successful!',
            });
            
          }
        });
    };

    async loadData(){
        let data = await getSupplierDetail(this.props.match.params.id);
        console.log(data.data);
        this.setState({
            dataSource: data.data,
            addressData: data.data.address,
        });
    }

    render(){

        const formItemLayout = {
            labelCol: {
              xs: { span: 24 },
              sm: { span: 8 },
            },
            wrapperCol: {
              xs: { span: 24 },
              sm: { span: 16 },
            },
        };

        const tailFormItemLayout = {
            wrapperCol: {
              xs: {
                span: 24,
                offset: 0,
              },
              sm: {
                span: 16,
                offset: 8,
              },
            },
        };

        const { dataSource, addressData, label } = this.state;
        const { getFieldDecorator } = this.props.form;
        return (
            <div>
            <Form {...formItemLayout} onSubmit={this.checkError} >
                <Form.Item label={label.name}>
                    {getFieldDecorator('name', {
                        initialValue: dataSource.name,
                        rules: [
                            {
                                required: true,
                                message: 'Please confirm your name!',
                            },
                        ],
                    })(<Input style={{width: '50%'}}/>)}
                </Form.Item>

                <Form.Item label={label.deliveryDetails}>
                    {getFieldDecorator('deliveryDetails', {
                        initialValue: dataSource.deliveryDetails,
                        rules: [
                            {
                                required: true,
                                message: 'Please confirm your Delivery Details!',
                            },
                        ],
                    })(<Input style={{width: '50%'}}/>)}
                </Form.Item>

                <Form.Item label={label.contactPersons}>
                    {getFieldDecorator('contactPersons', {
                        initialValue: dataSource.contactPersons,
                        rules: [
                            {
                                required: true,
                                message: 'Please confirm your Contact Persons!',
                            },
                        ],
                    })(<Input style={{width: '50%'}}/>)}
                </Form.Item>

                <Form.Item label={label.creditLine}>
                    {getFieldDecorator('creditLine', {
                        initialValue: dataSource.creditLine,
                        rules: [
                            {
                                required: true,
                                message: 'Please confirm your Credit Line!',
                            },
                        ],
                    })(<Input style={{width: '50%'}}/>)}
                </Form.Item>

                <Form.Item label="Address Details:" />
                <Form.Item label={label.unitNumber}>
                    {getFieldDecorator('unitNumber', {
                        initialValue: addressData.unitNumber,
                        rules: [
                            {
                                required: true,
                                message: 'Please confirm your Unit Number!',
                            },
                        ],
                    })(<Input style={{width: '50%'}}/>)}
                </Form.Item>

                <Form.Item label={label.streetNumber}>
                    {getFieldDecorator('streetNumber', {
                        initialValue: addressData.streetNumber,
                        rules: [
                            {
                                required: true,
                                message: 'Please confirm your Street Number!',
                            },
                        ],
                    })(<Input style={{width: '50%'}}/>)}
                </Form.Item>

                <Form.Item label={label.streetName}>
                    {getFieldDecorator('streetName', {
                        initialValue: addressData.streetName,
                        rules: [
                            {
                                required: true,
                                message: 'Please confirm your Street Name!',
                            },
                        ],
                    })(<Input style={{width: '50%'}}/>)}
                </Form.Item>

                <Form.Item label={label.suburb}>
                    {getFieldDecorator('suburb', {
                        initialValue: addressData.suburb,
                        rules: [
                            {
                                required: true,
                                message: 'Please confirm your Suburb!',
                            },
                        ],
                    })(<Input style={{width: '50%'}}/>)}
                </Form.Item>

                <Form.Item label={label.state}>
                    {getFieldDecorator('state', {
                        initialValue: addressData.state,
                        rules: [
                            {
                                required: true,
                                message: 'Please confirm your State!',
                            },
                        ],
                    })(<Input style={{width: '50%'}}/>)}
                </Form.Item>

                <Form.Item label={label.postcode}>
                    {getFieldDecorator('postcode', {
                        initialValue: addressData.postcode,
                        rules: [
                            {
                                required: true,
                                message: 'Please confirm your Postcode!',
                            },
                        ],
                    })(<Input style={{width: '50%'}}/>)}
                </Form.Item>

                <Form.Item label={label.country}>
                    {getFieldDecorator('country', {
                        initialValue: addressData.country,
                        rules: [
                            {
                                required: true,
                                message: 'Please confirm your Country!',
                            },
                        ],
                    })(<Input style={{width: '50%'}}/>)}
                </Form.Item>

                <Form.Item {...tailFormItemLayout}>
                    <Button style={{width: '50%'}} onClick={this.handleSubmit} type="primary" htmlType="submit"> {this.buttonName} </Button>
                </Form.Item>
            </Form>

            </div>
        );
    }
}

export default Form.create()(SuppliersDetails);
