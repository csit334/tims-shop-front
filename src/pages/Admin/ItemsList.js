
import React from 'react';
import { Table, Input, Button, Popconfirm, Divider,Radio } from 'antd';
import styles from './AdminPage.less';

import {getItems, deleteItem} from '@/services/itemService';

import { push } from 'react-router-redux';
import router from 'umi/router';

const Model_Area = ['All', 'Cars', 'Trains', 'Boats', 'Aircrafts', 'Others'];

const Search = Input.Search;

class ItemsList extends React.Component {
  constructor(props) {
    super(props);
    this.columns = [
      {
        title: 'ID',
        dataIndex: 'id',
        width: '4%',
        sorter: (a, b) => a.id - b.id,
      },
      {
        title: 'Item Name',
        dataIndex: 'name',
        width: '12.5%',
      },
      {
        title: 'Model Type',
        dataIndex: 'modelType',
        width: '12.5%',
        filters: [
          {
            text: 'Static',
            value: 'Static',
          },
          {
            text: 'Working',
            value: 'Working',
          },
          {
            text: 'Display',
            value: 'Display',
          },
        ],
        filterMultiple: false,
        onFilter: (value, record) => record.modelType.indexOf(value) === 0,
      },
      {
        title: 'Stock',
        dataIndex: 'stock',
        width: '8%',
        sorter: (a, b) => a.stock - b.stock,
      },
      {
        title: 'Price',
        dataIndex: 'price',
        width: '8%',
        sorter: (a, b) => a.price - b.price,
      },
      {
        title: 'Location',
        dataIndex: 'location',
        width: '8%',
      },
      {
        title: 'Operation',
        dataIndex: 'operation',
        width: '8%',
        render: (text, record) =>
          this.state.dataSource.length >= 1 ? (
            <span>
              <Popconfirm title="Sure to delete?" onConfirm={() => deleteItem(record.id).then(() => this.componentDidMount())}>
                <a href="javascript:;">Delete</a>
              </Popconfirm>
              <Divider type="vertical" />
              <a onClick={() => {router.push(`/admin/ItemD/${record.id}`)}}>Edit</a>
            </span>
          ) : null,
      },
    ];

    this.state = {
      dataSource: [],
      dataShow: [],
      is_Area: 'All',
      saveData: [],
    };

  }

  componentDidMount() {
    const {dataSource} = this.state;
    this.loadData();
    // this.setState({
    //   dataShow: dataSource,
    // });
  }

    // Create page
    handleCreate = () =>{
      router.push('/admin/ItemD');
    };

  async loadData(){
    let data = await getItems()
    //console.log(data);
    this.setState({
      dataSource: data.data,
      dataShow: data.data,
      saveData: data.data,
    })
  }

  onModelArea = e => {
    this.setState({
      is_Area: e.target.value,
    });
    if(e.target.value === 'Cars'){
      this.setState({
        dataShow: this.state.dataSource.filter(item => item.subjectArea === e.target.value),
        saveData: this.state.dataShow,
      });
    }
    else if(e.target.value === 'Trains'){
      this.setState({
        dataShow: this.state.dataSource.filter(item => item.subjectArea === e.target.value),
        saveData: this.state.dataShow,
      });
    }
    else if(e.target.value === 'Boats'){
      this.setState({
        dataShow: this.state.dataSource.filter(item => item.subjectArea === e.target.value),
        saveData: this.state.dataShow,
      });
    }
    else if(e.target.value === 'Aircrafts'){
      this.setState({
        dataShow: this.state.dataSource.filter(item => item.subjectArea === e.target.value),
        saveData: this.state.dataShow,
      });
    }
    else if(e.target.value === 'Others'){
      this.setState({
        dataShow: this.state.dataSource.filter(item => item.subjectArea === e.target.value),
        saveData: this.state.dataShow,
      });
    }
    else{
      this.setState({
        dataShow: this.state.dataSource,
        saveData: this.state.dataShow,
      });
    }
  };

  onSearch = (value) =>{
    if(!value){
      this.setState({
        dataShow: this.state.saveData,
        
      });
    }
    else{
      this.setState({
        dataShow: this.state.dataShow.filter(item => item.name === value),
      });
    }
  }

  render() {
    const { dataSource, dataShow } = this.state;
    const columns = this.columns.map(col => {
      return {
        ...col,
        onCell: record => ({
          record,
          dataIndex: col.dataIndex,
          title: col.title,
        }),
      };
    });

    return (
      <div className={styles.divLayout}>
        <h1>items List</h1>
      <div class="ant-card ant-card-bordered">
        <span>
          <Search 
            style={{marginTop: '20px', marginLeft: '20px', marginRight: '20px', width: '300px'}} 
            placeholder="Search By Item Name" 
            onSearch={
              value => this.onSearch(value)
            } 
            enterButton
          />
          <div>
            <Radio.Group style={{marginTop: '20px', marginLeft: '20px'}} options={Model_Area} 
              onChange={this.onModelArea}
              value={this.state.is_Area}
            />
          </div>
        </span>

        <Table style={{marginTop: '20px', marginLeft: '20px', marginRight: '20px'}}
          rowClassName={() => 'editable-row'}
          bordered
          dataSource={dataShow}
          columns={columns}
        />

        <div style={{textAlign:'center'}}>
          <Button onClick={this.handleCreate} style={{ marginBottom: '20px', marginTop: 16, width: '300px' }}>
            Create New Item
          </Button>
        </div>

      </div>
      </div>
    );
  }
}

export default ItemsList;

