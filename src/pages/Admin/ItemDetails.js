
import React from 'react';
import { Form, Input, Button, Modal } from 'antd';
import {getItemDetail, postItems, updateItems} from '@/services/itemService';
import { supportsHistory } from 'history/DOMUtils';
import { push } from 'react-router-redux';

class ItemDetails extends React.Component {
    constructor(props) {
        super(props);

        this.update = true;//
        this.buttonName = 'Update';

        this.state = {
            dataSource: {},
            label: {
                name: 'Name',
                modelType: 'Model Type',
                subjectArea: 'Subject Area',
                stock: 'Stock',
                price: 'Price',
                instockDate: 'In Stock Date',
                location: "Location",
                description: "Description",
                available: "Available",
            },
        };

        //
        if(this.props.match.params.id){
          this.update = true;
          this.buttonName = 'Update';
        }
        else {
          this.update = false;
          this.buttonName = 'Create';
        }
    }

    componentDidMount() {
      if(this.update){ //
        this.loadData();
    }
    }

    handleSubmit = () => {
        var timestamp = new Date().getTime();
      let data = {
        name: this.props.form.getFieldValue('name'),
        modelType: this.props.form.getFieldValue('modelType'),
        subjectArea: this.props.form.getFieldValue('subjectArea'),
        stock: this.props.form.getFieldValue('stock'),
        price: this.props.form.getFieldValue('price'),
        instockDate: timestamp,
        location: this.props.form.getFieldValue('location'),
        description: this.props.form.getFieldValue('description'),
        available: this.props.form.getFieldValue('available'),
      };
      console.log(data);

      if(this.update){ // update
          data.id = this.state.dataSource.id;
          updateItems(data).then(
              this.props.history.push('/admin/ItemsL')
          );
          Modal.success({
            title: 'Successful',
            content: 'Your Update Is Successful!',
          });
      }
      else { // create
          postItems(data).then(
            this.props.history.push('/admin/ItemsL')
          );
      }
        this.componentDidMount();
    }

  checkError = e => {
      e.preventDefault();
      this.props.form.validateFieldsAndScroll((err, values) => {
        if (!err) {
          console.log('Received values of form: ', values);
          Modal.success({
            title: 'Successful',
            content: 'Your Create Is Successful!',
          });
        }
      });
  };


    async loadData(){
        const {dataSource} = this.state;
        let data = await getItemDetail(this.props.match.params.id);
        console.log(data.data);
        this.setState({
          dataSource: data.data,
        })
    }

    render(){

        const formItemLayout = {
            labelCol: {
              xs: { span: 24 },
              sm: { span: 8 },
            },
            wrapperCol: {
              xs: { span: 24 },
              sm: { span: 16 },
            },
          };

          const tailFormItemLayout = {
            wrapperCol: {
              xs: {
                span: 24,
                offset: 0,
              },
              sm: {
                span: 16,
                offset: 8,
              },
            },
        };

        const { dataSource, label } = this.state;
        const { getFieldDecorator } = this.props.form;
        return (
            <div>
            <Form {...formItemLayout} onSubmit={this.checkError}>
                <Form.Item label={label.name}>
                  {getFieldDecorator('name', {
                        initialValue: dataSource.name,
                        rules: [
                            {
                                required: true,
                                message: 'Please confirm your name!',
                            },
                        ],
                    })(<Input style={{width: '50%'}}/>)}
                </Form.Item>

                <Form.Item label={label.modelType}>
                    {getFieldDecorator('modelType', {
                        initialValue: dataSource.modelType,
                        rules: [
                            {
                                required: true,
                                message: 'Please confirm your modelType!',
                            },
                        ],
                    })(<Input style={{width: '50%'}}/>)}
                </Form.Item>

                <Form.Item label={label.subjectArea}>
                    {getFieldDecorator('subjectArea', {
                        initialValue: dataSource.subjectArea,
                        rules: [
                            {
                                required: true,
                                message: 'Please confirm your subjectArea!',
                            },
                        ],
                    })(<Input style={{width: '50%'}}/>)}
                </Form.Item>

                <Form.Item label={label.stock}>
                    {getFieldDecorator('stock', {
                        initialValue: dataSource.stock,
                        rules: [
                            {
                                required: true,
                                message: 'Please confirm your stock!',
                            },
                        ],
                    })(<Input style={{width: '50%'}}/>)}
                </Form.Item>

                <Form.Item label={label.price}>
                  {getFieldDecorator('price', {
                        initialValue: dataSource.price,
                        rules: [
                            {
                                required: true,
                                message: 'Please confirm your price!',
                            },
                        ],
                    })(<Input style={{width: '50%'}}/>)}
                </Form.Item>

                <Form.Item label={label.location}>
                    {getFieldDecorator('location', {
                        initialValue: dataSource.location,
                        rules: [
                            {
                                required: true,
                                message: 'Please confirm your location!',
                            },
                        ],
                    })(<Input style={{width: '50%'}}/>)}
                </Form.Item>

                <Form.Item label={label.description}>
                    {getFieldDecorator('description', {
                        initialValue: dataSource.description,
                        rules: [
                            {
                                required: true,
                                message: 'Please confirm your description!',
                            },
                        ],
                    })(<Input style={{width: '50%'}}/>)}
                </Form.Item>

                <Form.Item label={label.available}>
                    {getFieldDecorator('available', {
                        initialValue: dataSource.available,
                        rules: [
                            {
                                required: true,
                                message: 'Please confirm your available!',
                            },
                        ],
                    })(<Input style={{width: '50%'}}/>)}
                </Form.Item>

                <Form.Item {...tailFormItemLayout}>
                    <Button style={{width: '50%'}} onClick={this.handleSubmit} type="primary" htmlType="submit"> {this.buttonName} </Button>
                </Form.Item>

            </Form>
            </div>
        );
    }
}

export default Form.create()(ItemDetails);
