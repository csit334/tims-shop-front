
import React, {PureComponent} from 'react';
import { Select, InputNumber, Button, Table, Popconfirm, Modal, Input } from 'antd';

import {getSuppliers} from '@/services/supplierService';
import {getItems} from '@/services/itemService';
import {postSupplyOrder} from '@/services/supplyService';

import { push } from 'react-router-redux';

import styles from './AdminPage.less';

const Option = Select.Option;

var checkItemSelect = false;

class AddSupplyTransaction extends PureComponent {

    constructor(props) {
        super(props);
        this.columns = [
            {
                title: 'key',
                dataIndex: 'key',
            },
            {
                title: 'Supplier Name',
                dataIndex: 'supplier_name',
            },
            {
                title: 'Item Name',
                dataIndex: 'name',
            },
            {
                title: 'Purchase Quantity',
                dataIndex: 'purchase_quantity',
            },
            {
                title: 'Purchase Total Price($)',
                dataIndex: 'purchase_totalPrice',
            },
            {
                title: 'Operation',
                dataIndex: 'operation',
                width: '8%',
                render: (text, record) =>
                    this.state.dataSource.length >= 1 ? (
                        <Popconfirm title="Sure to delete?" onConfirm={() => this.handleDelete(record.key)}>
                            <a href="javascript:;">Delete</a>
                        </Popconfirm>
                    ) : null,
            },
        ];

        this.STColumns = [
            {
                title: 'Supplier Name',
                dataIndex: 'supplier_name',
            },
            {
                title: 'Item Name',
                dataIndex: 'name',
            },
            {
                title: 'Purchase Quantity',
                dataIndex: 'purchase_quantity',
            },
            {
                title: 'Purchase Total Price($)',
                dataIndex: 'purchase_totalPrice',
            },
        ];

        this.state = {
            dataSource: [],
            itemData: [],
            get_itemName:'',
            count: 0,
            quantity: 0,
            supplierName: '',
            visible: false,
            sale_totalPrice: 0,

            itemDataList: [],
            supplierDataList: [],
            get_adminId: '',
            itemDataRecord: [],
            get_supplierId: 0,
        };
    }

    componentDidMount() {
        this.loadData();
    }

    async loadData(){
        let itemdata = await getItems();
        let supplierdata = await getSuppliers();
        this.setState({
            itemDataList: itemdata.data,
            supplierDataList: supplierdata.data,
        });
    }    

    // 添加Item的浮窗的操作...
    showModal = () => {
        this.setState({
            visible: true,
        });
    };
    handleCancel = e => {
        //console.log(e);
        this.setState({
            visible: false,
        });
    };
    // ...

    // 获取Supplier
    onChangeS = (value) => {
        
        //console.log(`selected ${value}`);
        this.setState({
            supplierName: value,
        });
    };

    // 获取数量Input Number
    onChangeQ = (value) => {
        //console.log('changed', value);
        this.setState({
            quantity: value,
        });
    };

    // Select的功能
    onChange = (value) => {
        //console.log(`selected ${value}`);
        this.setState({
            get_itemName: value,
        });
        checkItemSelect = true;
    };

    onBlur = () => {
        //console.log('blur');
    };

    onFocus = () => {
        //console.log('focus');
    };

    onSearch = (val) => {
        //console.log('search:', val);
    };
    // ...

    onGetAdminID = (value) => {
        this.setState({
            get_adminId: value,
        });
    }

    handleAdd = () => {
        const { dataSource, itemDataRecord, get_itemName, count, quantity, supplierName, itemDataList
        } = this.state;
        
        if(checkItemSelect!==true){
            Modal.error({
                title: 'ERROR',
                content: 'Please select a item/another a item',
            });
        }
        else{
            var dataFind = itemDataList.filter(item => item.name === get_itemName);
            dataFind.key = count;
            // get price
            var get_price = dataFind.find((i) => i.name === get_itemName).price;
            var item_id = dataFind.find((i) => i.name === get_itemName).id;
        
            const dt = {
                key: count,// 给每个添加一个唯一的标记
                model_item_id: dataFind.find((i) => i.name === get_itemName).id,
                name: dataFind.find((i) => i.name === get_itemName).name,
                purchase_price: get_price,
                purchase_totalPrice: quantity * get_price,
                purchase_quantity: quantity,

                supplier_name: supplierName,
            };

            // save the data of items:[{}]
            const itemsData = {
                modelItemId: item_id,
                unitPrice: get_price,
                quantity: quantity,
            };

            this.setState({
                dataSource: [...dataSource, dt],
                count: count + 1,

                itemDataRecord: [...itemDataRecord, itemsData],
            });

            checkItemSelect = false;

            this.calculateTotalPrice(dt.purchase_totalPrice);

        }
        
    };
    
    //000
    onSumbit = e => {
        const {supplierDataList} = this.state;
        var timestamp = new Date().getTime();
        let data = {
            supplierId: this.state.get_supplierId,
            adminId: this.state.get_adminId,
            datetime: timestamp,
            items: this.state.itemDataRecord,
            price: this.state.sale_totalPrice,
        };
        var dataSupplierFind = supplierDataList.filter(item => item.name === this.state.supplierName);
        var sId = dataSupplierFind.find((i) => i.name === this.state.supplierName).id;
        
        this.setState({
            get_supplierId: sId,
        });

        postSupplyOrder(data).then(
            this.props.history.push('/admin/SalesO')
        );
        Modal.success({
            title: 'Successful',
            content: 'The Sale Is Successful!',
        });
        this.clearInfo();
    };
    clearInfo = () => {
        this.setState({
            dataSource: [],
            itemData: [],
            get_itemName:'',
            count: 0,
            quantity: 0,
            supplierName: '',
            visible: false,
            sale_totalPrice: 0,

            get_adminId: '',
            itemDataRecord: [],
            get_supplierId: 0,
        });
    };

    handleDelete = key => {
        const dataSource = [...this.state.dataSource];
        const itemDataRecord = [...this.state.itemDataRecord];
        this.setState({ 
            dataSource: dataSource.filter(item => item.key !== key),
            itemDataRecord: itemDataRecord.filter(item => item.orderId != key),
        });
    };

    calculateTotalPrice = (value) => {
        const {sale_totalPrice} = this.state;
        this.setState({ 
            sale_totalPrice: sale_totalPrice + value,
        }); 
    }

    render() {
        const {
            dataSource,
            supplierName,
            itemData,
            sale_totalPrice,
            itemDataList,
            supplierDataList
        } = this.state;

        const columns = this.columns.map(col => {
            return {
              ...col,
              onCell: record => ({
                record,
                dataIndex: col.dataIndex,
                title: col.title,
              }),
            };
        });
        const STColumns = this.STColumns.map(col => {
            return {
              ...col,
              onCell: () => ({
                dataIndex: col.dataIndex,
                title: col.title,
              }),
            };
        });

        return(
            <div className={styles.divLayout}>
                <h1>Create Supply Transaction</h1>
                <div class="ant-card ant-card-bordered">

                <div style={{ marginLeft: '10px', marginTop: '10px', marginBottom: '10px' }}>
                    <span>Input Admin Id: <InputNumber onChange={this.onGetAdminID} /></span>
                </div>

                <Select
                    showSearch
                    style={{ width: '30%', marginLeft: '10px', marginRight: '10px', marginBottom: '10px' }}
                    placeholder="Select a item"
                    optionFilterProp="children"
                    onChange={this.onChangeS}
                    onFocus={this.onFocus}
                    onBlur={this.onBlur}
                    onSearch={this.onSearch}
                    filterOption={(input, option) =>
                        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                    }
                >
                    {supplierDataList.map(item => (
                        <Option value={item.name}>{item.name}</Option>
                    ))}
                </Select>

                <Select
                    showSearch
                    style={{ width: '30%', marginRight: '10px' }}
                    placeholder="Select a item"
                    optionFilterProp="children"
                    onChange={this.onChange}
                    onFocus={this.onFocus}
                    onBlur={this.onBlur}
                    onSearch={this.onSearch}
                    filterOption={(input, option) =>
                        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                    }
                >
                    {itemDataList.map(item => (
                        <Option value={item.name}>{item.name}</Option>
                    ))}
                </Select>

                <InputNumber style={{ marginRight: '10px' }} onChange={this.onChangeQ} />

                <Button type="primary" onClick={this.handleAdd}> Add </Button>

                </div>
                
                <div class="ant-card ant-card-bordered">
                    <Table style={{marginBottom: '20px', marginTop: '20px', marginLeft: '20px', marginRight: '20px'}}
                        rowClassName={() => 'editable-row'}
                        bordered
                        dataSource={dataSource}
                        columns={columns}
                    />
                    <div style={{textAlign:'center'}}>
                        <Button onClick={this.showModal} style={{ marginBottom: '20px', marginTop: 16, width: '300px' }}>
                            Sumbit
                        </Button>
                    </div>
                </div>
                
                <Modal
                    title="Record Confirm"
                    visible={this.state.visible}
                    onOk={this.onSumbit}
                    onCancel={this.handleCancel}
                >   
                    <Table columns={STColumns} dataSource={dataSource} size="middle" pagination={false} />
                    <div style={{marginRight: '20px', marginTop: '10px', textAlign:'right'}}> ${sale_totalPrice} </div>
                </Modal>

            </div>
        );
    }
}

export default AddSupplyTransaction;
