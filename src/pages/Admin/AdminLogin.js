import React, { Component } from 'react';
import Link from 'umi/link';
import { Alert, message, Form } from 'antd';
import Login from 'ant-design-pro/lib/Login';
import styles from '../User/Login.less';
import { push } from 'react-router-redux';
import {getAdmins} from '@/services/adminService';

const { UserName, Password, Submit } = Login;

function getIndex(value, arr, prop) { 
  for(var i = 0; i < arr.length; i++) { 
   if(arr[i][prop] === value) { 
    return i; 
   } 
  } 
  return -1; //to handle the case where the value doesn't exist 
} 

class AdminLoginPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      notice:'',
      type: 'account',
      dataSource: [],
    };
  }

  componentDidMount() {
    this.loadData();
  }

  onSubmit = (err, values) => {
    console.log('value collected ->', {
      ...values,
    });
    if (this.state.type === 'account') {
      this.setState(
        {
          notice: '',
        },
        () => {
          var AdminUserName = getIndex(values.account, this.state.dataSource, 'username');
          var AdminPassword = getIndex(values.password, this.state.dataSource, 'password');
          if (!err && ( AdminUserName < 0 || AdminPassword < 0) ) {
              setTimeout(() => {
                this.setState({
                  notice: 'The combination of username or password is incorrect!',
                });
              }, 500);
          }
          else if(!err){this.props.history.push('/admin');}
        }
      );
    }
  };

  //
  async loadData(){
    let data = await getAdmins()
    this.setState({
      dataSource: data.data,
    })
  }

  render() {
    
    return (
      <div className={styles.main}>
        <Login
          defaultActiveKey={this.state.type}
          onSubmit={this.onSubmit}
          ref={form => {
            this.loginForm = form;
          }}
          className={styles.login}
        >
            {this.state.notice && (
              <Alert
                style={{ marginBottom: 24 }}
                message={this.state.notice}
                type="error"
                showIcon
                closable
              />
            )}

            <UserName name="account" placeholder="Username"
              rules={[
                {
                  required: true,
                  message: "Please enter admin username",
                },
              ]}
              onPressEnter={e => {
                e.preventDefault();
                this.loginForm.validateFields(this.onSubmit);
              }}
            />

            <Password name="password" placeholder="Password"
              rules={[
                {
                  required: true,
                  message: "Please enter password",
                },
              ]}
              onPressEnter={e => {
                e.preventDefault();
                this.loginForm.validateFields(this.onSubmit);
              }}
            />

            <Submit className={styles.submit}>
              Login
            </Submit>

          </Login>
          
        </div>
    );
  }
}

export default Form.create()(AdminLoginPage);
