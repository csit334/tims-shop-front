import React, { Component } from 'react';
import Link from 'umi/link';
import { Alert, message, Form } from 'antd';
import Login from 'ant-design-pro/lib/Login';
import styles from '../User/Login.less';
import { push } from 'react-router-redux';

const { UserName, Password, Submit } = Login;

class StoreControlLogin extends React.Component {
  state = {
    notice:'',
    type: 'account',
  };

  onSubmit = (err, values) => {
    console.log('value collected ->', {
      ...values,
    });
    if (this.state.type === 'account') {
      this.setState(
        {
          notice: '',
        },
        () => {
            if(!err && (values.account !== 'Boss' || values.password !== '8888') ){
              setTimeout(() => {
                this.setState({
                  notice: 'The combination of username or password is incorrect!',
                });
              }, 500);
            }
            else if(!err){this.props.history.push('/admin/AdminL');}
        }
      );
    }
  };

  render() {

    return (
      <div className={styles.main}>
        <Login
          defaultActiveKey={this.state.type}
          onSubmit={this.onSubmit}
          ref={form => {
            this.loginForm = form;
          }}
          className={styles.login}
        >   
            <div style={{marginBottom:'50px'}}>
            </div>
            {this.state.notice && (
              <Alert
                style={{ marginBottom: '24px' }}
                message={this.state.notice}
                type="error"
                showIcon
                closable
              />
            )}

            <UserName name="account" placeholder="Username"
              rules={[
                {
                  required: true,
                  message: "Please enter admin account",
                },
              ]}
              onPressEnter={e => {
                e.preventDefault();
                this.loginForm.validateFields(this.onSubmit);
              }}
            />
            <Password name="password" placeholder="Password" 
              rules={[
                {
                  required: true,
                  message: "Please enter password!",
                },
              ]}
              onPressEnter={e => {
                e.preventDefault();
                this.loginForm.validateFields(this.onSubmit);
              }}
            />

          <Submit className={styles.submit}>
                Login
          </Submit>
          
        </Login>
</div>
    );
  }
}

export default Form.create()(StoreControlLogin);