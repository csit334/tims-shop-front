
import React from 'react';
import { Form, Input, Table } from 'antd';
import {getSalesOrderDetail} from '@/services/saleService';
import { supportsHistory } from 'history/DOMUtils';
import styles from './AdminPage.less';

class SaleOrderDetails extends React.Component {
    constructor(props) {
        super(props);

        this.columns = [
            {
              title: 'Item ID',
              dataIndex: 'id',
              width: '5%',
            },
            {
              title: 'Model Item ID',
              dataIndex: 'modelItemId',
              width: '5%',
            },
            {
              title: 'Order ID',
              dataIndex: 'orderId',
              width: '5%',
            },
            {
              title: 'Item Price',
              dataIndex: 'price',
              width: '8%',
            },
            {
              title: 'Quantity',
              dataIndex: 'quantity',
              width: '8%',
            },
        ];

        this.state = {
            dataSource: {},
            itemsData: [],
            label: {
                adminId: 'Admin ID',
                discount: 'Discount',
                price: 'Total Price',
                state: 'State',
                customerId: 'Customer ID',
                datetime: 'Date Time',
            },
        };
    }

    componentDidMount() {
        this.loadData();
    }

    async loadData(){
        const {dataSource} = this.state;
        let data = await getSalesOrderDetail(this.props.match.params.id);
        console.log(data.data);
        this.setState({
          dataSource: data.data,
          itemsData: data.data.items,
        })
    }

    render(){
        const columns = this.columns.map(col => {
            return {
              ...col,
              onCell: record => ({
                record,
                dataIndex: col.dataIndex,
                title: col.title,
              }),
            };
        });

        const formItemLayout = {
            labelCol: {
              xs: { span: 24 },
              sm: { span: 8 },
            },
            wrapperCol: {
              xs: { span: 24 },
              sm: { span: 16 },
            },
          };

        const { dataSource, itemsData, label } = this.state;
        const { getFieldDecorator } = this.props.form;
        return (
            <div className={styles.divLayout}>
            <Form {...formItemLayout}>
                <Form.Item label={label.adminId}>
                    <Input style={{width: '40%'}} value={dataSource.adminId}/>
                </Form.Item>
                <Form.Item label={label.customerId}>
                    <Input style={{width: '40%'}} value={dataSource.customerId}/>
                </Form.Item>
                <Form.Item label={label.discount}>
                    <Input style={{width: '40%'}} value={dataSource.discount}/>
                </Form.Item>
                <Form.Item label={label.price}>
                    <Input style={{width: '40%'}} value={dataSource.price}/>
                </Form.Item>
                <Form.Item label={label.state}>
                    <Input style={{width: '40%'}} value={dataSource.state}/>
                </Form.Item>
                <Form.Item label={label.datetime}>
                    <Input style={{width: '40%'}} value={dataSource.datetime}/>
                </Form.Item>
            </Form>

            <h1>Sale Item Order Details</h1>

              <div class="ant-card ant-card-bordered">
                <Table style={{marginTop: '20px', marginLeft: '20px', marginRight: '20px'}}
                    rowClassName={() => 'editable-row'}
                    bordered
                    dataSource={itemsData}
                    columns={columns}
                />
              </div>

            </div>
        );
    }
}

export default Form.create()(SaleOrderDetails);
