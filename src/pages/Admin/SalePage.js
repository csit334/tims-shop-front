
import React, {PureComponent} from 'react';
import { Select, InputNumber, Button, Table, Popconfirm, Modal, Radio, Input } from 'antd';

import {getCustomers} from '@/services/customerService';
import {getItems} from '@/services/itemService';
import {postSalesOrder} from '@/services/saleService';

import { push } from 'react-router-redux';

import styles from './AdminPage.less';

const Option = Select.Option;
const RadioGroup = Radio.Group;

var checkItemSelect = false;

const Member_Yes_No = ['No','Yes'];

class SalePage extends PureComponent {

    constructor(props) {
        super(props);
        this.columns = [
            {
                title: 'key',
                dataIndex: 'key',
            },
            {
                title: 'Item ID',
                dataIndex: 'model_item_id',
            },
            {
                title: 'Item Name',
                dataIndex: 'name',
            },
            {
                title: 'Quantity',
                dataIndex: 'quantity',
            },
            {
                title: 'Total Price($)',
                dataIndex: 'totalPrice',
            },
            {
                title: 'Operation',
                dataIndex: 'operation',
                width: '8%',
                render: (text, record) =>
                    this.state.dataSource.length >= 1 ? (
                        <Popconfirm title="Sure to delete?" onConfirm={() => this.handleDelete(record.key)}>
                            <a href="javascript:;">Delete</a>
                        </Popconfirm>
                    ) : null,
            },
        ];

        this.receiptColumns = [
            {
                title: 'Item ID',
                dataIndex: 'model_item_id',
            },
            {
                title: 'Item Name',
                dataIndex: 'name',
            },
            {
                title: 'Quantity',
                dataIndex: 'quantity',
            },
            {
                title: 'Total Price($)',
                dataIndex: 'totalPrice',
            },
        ];

        this.state = {
            dataSource: [],
            disabled: true,
            
            count: 0,
            quantity: 0,
            visible: false,
            p_visible: false,
            showInfoM: false,
            sale_totalPrice: 0,
            is_member: 'No',

            get_balance: 0,
            get_CustomerName: '',
            use_balcane: 0,

            itemDataList: [],
            customerDataList: [],
            get_itemName:'',
            get_username: '',
            get_customerId: '',
            get_adminId: '',
            itemDataRecord: [],
        };
    }

    componentDidMount() {
        this.loadData();
    }

    async loadData(){
        let itemdata = await getItems();
        let customerdata = await getCustomers();
        this.setState({
            itemDataList: itemdata.data,
            customerDataList: customerdata.data,
        });
    }

    // 添加Item的浮窗的操作...
    showModal = () => {
        this.setState({
            visible: true,
            //p_visible: false,
        });
    };

    onCheckMember = () =>{
        const { is_member, get_balance, get_CustomerName, get_username, use_balcane, customerDataList } = this.state;
        var memberData2 = customerDataList.filter(item => item.username === get_username);
        console.log('asdfasdfasdfaf:  ', memberData2);
        var a = 0;
        if(memberData2.length === 0){
            Modal.error({
                title: 'ERROR',
                content: 'The username is not exist',
            });
        }
        else{
            memberData2.find((i) => i.username === get_username).balance = memberData2.find((i) => i.username === get_username).balance - use_balcane;
            var get_b = memberData2.find((i) => i.username === get_username).balance;
            var get_c = memberData2.find((i) => i.username === get_username).lastName + ' ' + memberData2.find((i) => i.username === get_username).firstName;
            this.setState({
                showInfoM: true,
                get_balance: get_b,
                get_CustomerName: get_c,
                get_customerId: memberData2.find((i) => i.username === get_username).id,
            });
        }
    };

    useBalance = () => {
        this.setState({
            p_visible: true,
        });
    };

    showpModal = () => {
        this.setState({
            showInfoM: true,
        });
    };

    handleCancel = e => {
        //console.log(e);
        this.setState({
            visible: false,
            p_visible: false,
            showInfoM: false,
        });
    };
    // ...

    // 获取数量Input Number
    onChangeQ = (value) => {
        console.log('changed', value);
        this.setState({
            quantity: value,
        });
    };

    // Select的功能
    onChange = (value) => {
        console.log(`selected ${value}`);
        this.setState({
            get_itemName: value,
        });
        checkItemSelect = true;
    };

    onBlur = () => {
        console.log('blur');
    };

    onFocus = () => {
        console.log('focus');
    };

    onSearch = (val) => {
        console.log('search:', val);
    };
    // ...

    onGetMember = (value) => {
        console.log('changed', value);
        this.setState({
            get_username: value.target.value,
        });
    }

    onUserBalance = (value) => {
        console.log('changed', value);
        this.setState({
            use_balcane: value,
        });
    }
    onGetAdminID = (value) => {
        this.setState({
            get_adminId: value,
        });
    }

    onMember = e => {
        console.log('radio1 checked', e.target.value);
        if(e.target.value === 'Yes'){
          this.setState({
            is_member: e.target.value,
            disabled: !this.state.disabled,
          });
        }
        else{
          this.setState({
            use_balcane: 0,
            is_member: e.target.value,
            disabled: !this.state.disabled,
          });
        }
    };

    handleAdd = () => {
        const { dataSource, itemDataRecord, get_itemName, count, quantity, sale_totalPrice, itemDataList,
        } = this.state;

        if(checkItemSelect!==true){
            Modal.error({
                title: 'ERROR',
                content: 'Please select a item/another item',
            });
        }
        else{
            var dataFind = itemDataList.filter(item => item.name === get_itemName);
            dataFind.key = count;
            // get price
            var get_price = dataFind.find((i) => i.name === get_itemName).price;

            const dt = {
                key: count,// 给每个添加一个唯一的标记
                model_item_id: dataFind.find((i) => i.name === get_itemName).id,
                name: dataFind.find((i) => i.name === get_itemName).name,
                price: get_price,
                totalPrice: quantity * get_price,
                quantity: quantity,
            };

            // save the data of items:[{}]
            const itemsData = {
                //orderId: dataFind.find((i) => i.name === get_itemName).key,
                modelItemId: dataFind.find((i) => i.name === get_itemName).id,
                price: get_price,
                quantity: quantity,
            };

            this.setState({
                dataSource: [...dataSource, dt],
                count: count + 1,

                itemDataRecord: [...itemDataRecord, itemsData],
            });

            checkItemSelect = false;

            this.calculateTotalPrice(dt.totalPrice);
        }
        
    };
    
    //000
    onSumbit = e => {
        var customerI;
        if(this.state.is_member === 'Yes'){
            customerI = this.state.get_customerId;
        }
        else{
            customerI = null;
        }
        var timestamp = new Date().getTime();
        const {sale_totalPrice, use_balcane} = this.state;
        let data = {
            adminId: this.state.get_adminId,
            customerId: customerI,
            datetime: timestamp,
            discount: 0,
            items: this.state.itemDataRecord,
            price: sale_totalPrice - use_balcane,
            state: 'NORMAL',
        };
        postSalesOrder(data).then(
            this.props.history.push('/admin/SalesO')
        );
        Modal.success({
            title: 'Successful',
            content: 'The Sale Is Successful!',
        });
        this.clearInfo();
    };
    clearInfo = () => {
        this.setState({
            dataSource: [],
            count: 0,
            quantity: 0,
            visible: false,
            p_visible: false,
            showInfoM: false,
            sale_totalPrice: 0,
            is_member: 'No',

            get_balance: 0,
            get_CustomerName: '',
            use_balcane: 0,

            get_itemName:'',
            get_username: '',
            get_customerId: '',
            get_adminId: '',
            itemDataRecord: [],
        });
    };

    handleDelete = key => {
        const dataSource = [...this.state.dataSource];
        const itemDataRecord = [...this.state.itemDataRecord];
        this.setState({ 
            dataSource: dataSource.filter(item => item.key !== key),
            itemDataRecord: itemDataRecord.filter(item => item.orderId != key),
        });
    };

    calculateTotalPrice = (value) => {
        const {sale_totalPrice} = this.state;
        this.setState({ 
            sale_totalPrice: sale_totalPrice + value,
        }); 
    }

    render() {
        const {
            dataSource,
            sale_totalPrice,
            get_balance,
            get_CustomerName,
            use_balcane,

            itemDataList,
            customerDataList,
        } = this.state;

        const columns = this.columns.map(col => {
            return {
              ...col,
              onCell: record => ({
                record,
                dataIndex: col.dataIndex,
                title: col.title,
              }),
            };
        });
        const receiptColumns = this.receiptColumns.map(col => {
            return {
              ...col,
              onCell: () => ({
                dataIndex: col.dataIndex,
                title: col.title,
              }),
            };
        });

        return(
            <div className={styles.divLayout}>
                <h1>Create Sales</h1>
                <div class="ant-card ant-card-bordered">

                <div style={{ marginLeft: '10px', marginTop: '10px', marginBottom: '10px' }}>
                    Input Admin Id: <InputNumber style={{marginRight: '50px'}} onChange={this.onGetAdminID} />
                    Is The Customer A Member?
                    <RadioGroup style={{marginLeft: '10px'}} options={Member_Yes_No} onChange={this.onMember} value={this.state.is_member} />
                    <Input disabled={this.state.disabled} style={{marginRight: '10px', width: '10%'}} onChange={this.onGetMember}/>
                    <Button disabled={this.state.disabled} style={{marginRight: '10px', width: '10%'}} type="primary" onClick={this.onCheckMember}> Check </Button>
                    <Button disabled={this.state.disabled} style={{ width: '15%'}} type="primary" onClick={this.useBalance}> Use Balance </Button>
                    <span></span>
                </div>

                <Select
                    showSearch
                    style={{ width: '30%', marginLeft: '10px', marginRight: '10px', marginBottom: '10px' }}
                    placeholder="Select a item"
                    optionFilterProp="children"
                    onChange={this.onChange}
                    onFocus={this.onFocus}
                    onBlur={this.onBlur}
                    onSearch={this.onSearch}
                    filterOption={(input, option) =>
                        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                    }
                >
                    {itemDataList.map(item => (
                        <Option value={item.name}>{item.name}</Option>
                    ))}
                </Select>

                <InputNumber style={{ marginRight: '10px'}} onChange={this.onChangeQ} />

                <Button type="primary" onClick={this.handleAdd}> Add </Button>

                </div>
                
                <div class="ant-card ant-card-bordered">
                    <Table style={{marginBottom: '20px', marginTop: '20px', marginLeft: '20px', marginRight: '20px'}}
                        rowClassName={() => 'editable-row'}
                        bordered
                        dataSource={dataSource}
                        columns={columns}
                    />
                    <div style={{textAlign:'center'}}>
                        <Button onClick={this.showModal} style={{ marginBottom: '20px', marginTop: 16, width: '300px' }}>
                            Sumbit
                        </Button>
                    </div>
                </div>
                
                <Modal
                    title="Receipt"
                    visible={this.state.visible}
                    onOk={this.onSumbit}
                    onCancel={this.handleCancel}
                >
                    <Table columns={receiptColumns} dataSource={dataSource} size="middle" pagination={false} />
                    
                    <div style={{marginRight: '20px', marginTop: '10px', textAlign:'right'}}> ${sale_totalPrice - use_balcane}</div>

                </Modal>

                <Modal
                    title="Payment"
                    visible={this.state.p_visible}
                    onOk={this.handleCancel}
                    onCancel={this.handleCancel}
                >
                    <span>Use Balances: <InputNumber onChange={this.onUserBalance} /></span>
                </Modal>

                <Modal
                    title="Customer Information"
                    visible={this.state.showInfoM}
                    onOk={this.handleCancel}
                    onCancel={this.handleCancel}
                >
                    <div>Customer Name: {get_CustomerName} </div>
                    <div>Customer Balance: {get_balance} </div>
                </Modal>

            </div>
        );
    }
}

export default SalePage;
