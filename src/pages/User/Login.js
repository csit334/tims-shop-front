import React, { Component } from 'react';
import Link from 'umi/link';
import { Alert, message, Form } from 'antd';
import Login from 'ant-design-pro/lib/Login';
import styles from './Login.less';
import { push } from 'react-router-redux';
import {getCustomers} from '@/services/customerService';

const { UserName, Password, Submit } = Login;

function getIndex(value, arr, prop) { 
  for(var i = 0; i < arr.length; i++) { 
   if(arr[i][prop] === value) { 
    return i; 
   } 
  } 
  return -1; //to handle the case where the value doesn't exist 
} 

class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      notice:'',
      type: 'account',
      dataSource: [],
    };
  }

  componentDidMount() {
    this.loadData();
  }

  onSubmit = (err, values) => {
    console.log('value collected ->', {
      ...values,
    });
    if (this.state.type === 'account') {
      this.setState(
        {
          notice: '',
        },
        () => {
          // if (!err && (values.username !== 'user' || values.password !== '123') ) {
          //     setTimeout(() => {
          //       this.setState({
          //         notice: 'The combination of username or password is incorrect!',
          //       });
          //     }, 500);
          // }
          // else if(!err){this.props.history.push('/');}

          var CustomerUserName = getIndex(values.username, this.state.dataSource, 'username');
          if (!err && ( CustomerUserName < 0) ) {
              setTimeout(() => {
                this.setState({
                  notice: 'The combination of username or password is incorrect!',
                });
              }, 500);
          }
          else if(!err){this.props.history.push('/');}
        }
      );
    }
  };

  async loadData(){
    let data = await getCustomers()
    this.setState({
      dataSource: data.data,
    })
  }

  render() {

    return (
      <div className={styles.main}>
        <Login
          defaultActiveKey={this.state.type}
          onSubmit={this.onSubmit}
          ref={form => {
            this.loginForm = form;
          }}
          className={styles.login}
        >
            {this.state.notice && (
              <Alert
                style={{ marginBottom: 24 }}
                message={this.state.notice}
                type="error"
                showIcon
                closable
              />
            )}

            <UserName name="username" placeholder="Username"
              rules={[
                {
                  required: true,
                  message: "Please enter username!",
                },
              ]}
              onPressEnter={e => {
                e.preventDefault();
                this.loginForm.validateFields(this.onSubmit);
              }}
            />

          <div>
            <Link className={styles.register} to="/login/adminlogin">
                Go to Admin Login Page
            </Link>

          </div>

          <Submit className={styles.submit}>
                Login
          </Submit>
          
        </Login>
</div>
    );
  }
}

export default Form.create()(LoginPage);
