// ref: https://umijs.org/config/
import { primaryColor } from '../src/defaultSettings';

export default {
  plugins: [
    [
      'umi-plugin-react',
      {
        antd: true,
        dva: {
          hmr: true,
        },
        targets: {
          ie: 11,
        },
        locale: {
          enable: true, // default false
          default: 'en-US', // default 
          baseNavigator: true, // default true, when it is true, will use `navigator.language` overwrite default
        },
        dynamicImport: {
          loadingComponent: './components/PageLoading/index',
        },
      },
    ],
    [
      'umi-plugin-pro-block',
      {
        moveMock: false,
        moveService: false,
        modifyRequest: true,
        autoAddMenu: true,
      },
    ],
  ],
  targets: {
    ie: 11,
  },

  /**
   * 路由相关配置
   */
  routes: [
    // admin page
    {
      path: '/admin',
      component: '../layouts/AdminLayout',
      routes: [
        { path: '/admin', redirect: '/admin/adminpage' },
        { path: '/admin/adminpage', component: './Admin/AdminPage' },
        
        { path: '/admin/SalePage', name: 'Sale Page', component: './Admin/SalePage' },
        { path: '/admin/AddST', name: 'Add Supply Transaction', component: './Admin/AddSupplyTransaction' },

        // Sale Order
        { path: '/admin/SalesO', name: 'SalesOrder', component: './Admin/SalesOrder' },
        { path: '/admin/SalesOD', component: './Admin/SaleOrderDetails' },
        { path: '/admin/SalesOD/:id', component: './Admin/SaleOrderDetails' },

        // Supply Order
        { path: '/admin/SupplyO', name: 'SupplyOrder', component: './Admin/SupplyOrder' },
        { path: '/admin/SupplyOD', component: './Admin/SupplyOrderDetails' },
        { path: '/admin/SupplyOD/:id', component: './Admin/SupplyOrderDetails' },

        // Item
        { path: '/admin/ItemsL', name: 'ItemsList', component: './Admin/ItemsList' },
        { path: '/admin/ItemD', component: './Admin/ItemDetails' },
        { path: '/admin/ItemD/:id', component: './Admin/ItemDetails' },

        // Supplier
        { path: '/admin/SuppliersL', name: 'SuppliersList', component: './Admin/SuppliersList' },
        { path: '/admin/SupplierD', component: './Admin/SupplierDetails' },
        { path: '/admin/SupplierD/:id', component: './Admin/SupplierDetails' },

        // Customer
        { path: '/admin/CustomersL', name: 'CustomersList', component: './Admin/CustomersList' },
        { path: '/admin/CustomerD', component: './Admin/CustomerDetails' },
        { path: '/admin/CustomerD/:id', component: './Admin/CustomerDetails' },

        // Admin
        { path: '/admin/StoreCL', name: 'StoreControl', component: './Admin/StoreControlLogin' },
        { path: '/admin/AdminL', component: './Admin/AdminsList' },
        { path: '/admin/AdminD', component: './Admin/AdminDetails' },
        { path: '/admin/AdminD/:id', component: './Admin/AdminDetails' },
      ]
    },

    // login & register page
    {
      path: '/login',
      component: '../layouts/UserLayout',
      routes: [
        { path: '/login', redirect: '/login/user' },
        { path: '/login/user', name: 'login', component: './User/Login' },
        { path: '/login/adminlogin', name: 'Admin Login', component: './Admin/AdminLogin' },
      ],
    },

    // user home page
    {
      path: '/',
      component: '../layouts/BasicLayout',
      routes: [
        { path: '/', redirect: '/home' },
        {
          path: '/home',
          name: 'Home Page',
          icon: 'home',
          component: './UserPage/Home',
        },
        {
          path: '/cars',
          name: 'Cars',
          icon: 'car',
          component: './UserPage/Cars',
        },
        {
          path: '/trains',
          name: 'Trains',
          icon: 'smile',
          component: './UserPage/Trains',
        },
        {
          path: '/boats',
          name: 'Boats',
          icon: 'smile',
          component: './UserPage/Boats',
        },
        {
          path: '/aircrafts',
          name: 'Aircrafts',
          icon: 'smile',
          component: './UserPage/Aircrafts',
        },
        {
          path: '/others',
          name: 'Others',
          icon: 'smile',
          component: './UserPage/Others',
        },
        {
          path: '/account',
          component: './UserPage/Account',
        },
        {
          path: '/itemD',
          component: './ItemDetail',
        },
      ],
    },
    // user's account page

  ],
  disableRedirectHoist: true,

  /**
   * webpack 相关配置
   */
  define: {
    APP_TYPE: process.env.APP_TYPE || '',
  },
  // Theme for antd
  // https://ant.design/docs/react/customize-theme-cn
  theme: {
    'primary-color': primaryColor,
  },
  externals: {
    '@antv/data-set': 'DataSet',
  },
  ignoreMomentLocale: true,
  lessLoaderOptions: {
    javascriptEnabled: true,
  },
};
