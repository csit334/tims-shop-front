
const items = [
    {
        key: '',
        model_item_id: 'I001',
        item_name: 'Red Car 1',
        price: 100,
        totalPrice: 0,
        quantity: 0,
        supplier: 'uow',
        purchase_price: 20,
        purchase_quantity: 0,
        purchase_totalPrice: 0,


      model_type: 'Working',
      subject_area: 'Cars',
      stock: 10,
      instock_date: '2019-1-01',
      location: 'S20',
      description: 'Working model--come to make a car',
      avaliable: 'Yes',
    },
    {
        key: '',
        model_item_id: 'I002',
        item_name: 'Red Car 2',
        price: 10,
        totalPrice: 0,
        quantity: 0,
        supplier: 'big',
        purchase_price: 30,
        purchase_quantity: 0,
        purchase_totalPrice: 0,

        model_type: 'Working',
        subject_area: 'Cars',
        stock: 10,
        instock_date: '2019-1-01',
        location: 'S20',
        description: 'Working model--come to make a car',
        avaliable: 'Yes',
    },
  ];

  export default {
    'GET /item/itemD/getItem': items,
  };