
const fakeList = [
    {
        key: '',
        model_item_id: 'I001',
        item_name: 'Red Car 1',
        price: 100,
        totalPrice: 0,
        quantity: 0,
      },
]

let sourceData;

function getFakeList(req, res) {
    /*
  const params = req.query;

  const count = params.count * 1 || 20;

  const result = fakeList(count);
  */
 const result = fakeList;
  sourceData = result;
  return res.json(result);
}

function postFakeList(req, res) {
  const { /* url = '', */ body } = req;
  // const params = getUrlParams(url);
  const { method, key } = body;
  // const count = (params.count * 1) || 20;
  let result = sourceData;

  switch (method) {
    case 'delete':
      result = result.filter(item => item.id !== id);
      break;
    case 'update':
      result.forEach((item, i) => {
        if (item.key === key) {
          result[i] = Object.assign(item, body);
        }
      });
      break;
    case 'post':
      result.unshift({
        body,
        key: `${result.length}`,
        createdAt: new Date().getTime(),
      });
      break;
    default:
      break;
  }

  return res.json(result);
}

export default {
    'GET /api/fake_list': getFakeList,
    'POST /api/fake_list': postFakeList,
  };